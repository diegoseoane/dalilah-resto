# dalilah-resto
Aplicación para pedidos y delivery de comidas rápidas. 

## Instalación
Para su funcionamiento debe tener instalado "node". 

Busque en el [Sitio Oficial de Node](https://nodejs.org/es/download/) la mejor opción de acuerdo a su sistema operativo.

También debe instalar las siguientes dependencias:
~~~
npm install -g nodemon
npm i express
npm i js-yaml
npm i swagger-ui-express
~~~
y hacerla correr con: 
~~~
npm start
~~~